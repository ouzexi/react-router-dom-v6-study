import React, {useState} from "react";
import { NavLink, Outlet, useNavigate } from "react-router-dom";

export default function Message() {
    const [messages, setMessages] = useState([
        {id: '001', title: '消息1', content: '锄禾日当午'},
        {id: '002', title: '消息2', content: '锄禾日当2'},
        {id: '003', title: '消息3', content: '锄禾日当3'},
        {id: '004', title: '消息4', content: '锄禾日当4'}
    ])

    const navigate = useNavigate()
    function showDetail(item) {
      // v5可以用props.history 完成编程式导航
      // v6用 useNavigate
      // navigate('/about')

      // 第一个参数 路劲
      // 第二个参数 配置对象 只能传state，不能search/params
      navigate('detail', {
        replace: false,
        state: {
          id: item.id,
          title: item.title,
          content: item.content
        }
      })
    }

  return (
    <div>
      <ul>
        {
            messages.map((item) => {
                return (
                    // 路由链接
                    <li key={item.id}>
                    <NavLink to="detail"
                    state={{
                        id: item.id,
                        title: item.title,
                        content: item.content
                    }}>{item.title}</NavLink>&nbsp;&nbsp;
                    <button onClick={()=>showDetail(item)}>查看详情</button>
                    </li>
                )
            })
        }
      </ul>
      <hr/>
      {/* 指定路由组件的展示位置 */}
      <Outlet />
    </div>
  );
}
