import React from 'react'
import { useLocation } from 'react-router-dom'

export default function Detail() {
    // location接收state对象
    const {state} = useLocation()
    console.log("🚀 ~ file: Detail.jsx ~ line 7 ~ Detail ~ state", state)
    const id = state.id
    const title = state.title
    const content = state.content
  return (
    <ul>
        <li>消息的编号：{id}</li>
        <li>消息的标题：{title}</li>
        <li>消息的的内容:{content}</li>
    </ul>
  )
}
