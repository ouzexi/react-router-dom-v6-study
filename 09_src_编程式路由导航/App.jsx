import React from "react";
import { NavLink,  useRoutes } from 'react-router-dom'
import Header from "./components/Header";

import routes from './routes'
export default function App() {
    // 根据路由表生成对应的路由规则
    const element = useRoutes(routes)
  return (
    <div>
      <div className="row">
        {/* v5 路由组件有history等路由相关props，但一般组件无，可以借助withRouter */}
        {/* v6 两者都有hooks */}
        <Header />
      </div>
      <div className="row">
        <div className="col-xs-2 col-xs-offset-2">
          <div className="list-group">
            <NavLink className="list-group-item" to="/about">
              About
            </NavLink>
            {/* end可以当home的子路由激活时他自身不高亮 */}
            <NavLink className="list-group-item" end to="/home">
              Home
            </NavLink>
          </div>
        </div>
        <div className="col-xs-6">
          <div className="panel">
            <div className="panel-body">
                {/* 注册路由 */}
                {element}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
