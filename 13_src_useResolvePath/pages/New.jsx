import React from "react";
import { useResolvedPath } from 'react-router-dom'

export default function New() {
  const a = useResolvedPath('/user?id=001&name=tom#atguigu')
  // 解析路径 path search hash值
  console.log("🚀 ~ file: New.jsx ~ line 6 ~ New ~ a", a)
  
  return (
    <ul>
      <li>news001</li>
      <li>news002</li>
      <li>news003</li>
    </ul>
  );
}
