import React from "react";
import { NavLink, Outlet, useOutlet } from "react-router-dom";

export default function Home() {
  // 显示嵌套路由组件的内容 outlet内容还未挂载则null
  const a = useOutlet()
  console.log("🚀 ~ file: Home.jsx ~ line 6 ~ Home ~ a", a)

  return (
    <div>
      <h2>Home组件内容</h2>
      <div>
        <ul className="nav nav-tabs">
          <li>
            {/* <NavLink className="list-group-item" to="/home/news"> */}
            {/* 简写 */}
            <NavLink className="list-group-item" to="news">
              News
            </NavLink>
          </li>
          <li>
            <NavLink className="list-group-item" to="/home/message">
              Message
            </NavLink>
          </li>
        </ul>
        {/* 指定路由组件呈现的位置 */}
        <Outlet />
      </div>
    </div>
  );
}
